import wave
import numpy as np
import pandas as pd
import pickle
import pywt


class Signal(object):

    def __init__(self,
                 name,
                 data,
                 rr_bin_range=(234.85163198115271, 276.41687146297062),
                 p_height_range=(1.4044214049249117, 1.6578494444983445),
                 pp_bin_range=(231.13977553262845, 280.31128124840563),
                 pr_bin_range=(33.895661115441065, 52.440635275728425)
                 ):
        """
        Return a Signal object whose record name is *name*,
        signal data is *data*,
        RRInterval bin range is *mid_bin_range*
        """
        self.name = name
        self.sampling_rate = 300.  # 300 hz
        self.sampleFreq = 1 / 300

        # self.data = wave.discardNoise(data) # optimize this
        self.data = wave.filterSignalBios(data)
        # self.data = data

        self.RPeaks = wave.getRPeaks(self.data, sampling_rate=self.sampling_rate)
        if np.mean([self.data[i] for i in self.RPeaks]) < 0:  # signal is inverted
            self.data = -self.data

        self.baseline = wave.getBaseline(self)

        self.RRintervals = wave.interval(self.RPeaks)
        self.RRbinsN = wave.interval_bin(self.RRintervals, rr_bin_range)

        self.PWaves = wave.getPWaves(self)
        self.PHeights = np.asarray([self.data[i] - self.baseline for i in self.PWaves])
        minPHeight = 1.17975561806
        self.PHeights = np.add(self.PHeights, minPHeight)
        self.PHeights = np.square(self.PHeights)
        self.PHeightbinsN = wave.interval_bin(self.PHeights, p_height_range)

        self.PPintervals = wave.interval(self.PWaves)
        self.PPbinsN = wave.interval_bin(self.PPintervals, pp_bin_range)

        self.PRintervals = self.RPeaks[1:] - self.PWaves
        self.PRbinsN = wave.interval_bin(self.PRintervals, pr_bin_range)


def deriveBinEdges(training):
    """
    This function derives bin edges from the normal EKG signals

    Parameters
    ----------
    training : tuple
        tuple of lists of training data record names and labels, 2nd tuple from wave.getPartitionedRecords()

    Returns
    -------
    edges : tuple
        tuple of bin edge values, i.e. (230,270) to use as mid_bin_range in wave.interval_bin()
    """

    lower = 0
    upper = 0
    normals = []

    for idx, val in enumerate(training[0]):

        if training[1][idx] == 'N':
            normals.append(val)

    for i in normals:
        signal = getFeaturesHardcoded(i)

        tempMean = signal[56]
        tempStd = signal[57]

        lower += tempMean - tempStd
        upper += tempMean + tempStd

    lower = lower / len(normals)
    upper = upper / len(normals)

    return (lower, upper)


hardcoded_features = pd.read_csv("hardcoded_features.csv")


def getFeaturesHardcoded(name):
    """
    this function extract the features from the attributes of a signal
    it uses the hardcoded csv data for each signal that we saved earlier using saveSignalFeatures()

    Parameters
    ----------
    name : String
        record name

    Returns
    -------
    features : array_like
        a feature array for the given signal

    """

    signal = np.asarray(hardcoded_features.loc[hardcoded_features['0'] == name])[0]

    return signal[2:]


def createcolnames(n_features, feature_name):
    feature_list = []
    counter = 1
    if n_features == 1:
        return [feature_name]
    else:
        for i in range(n_features):
            feature_list.append(feature_name + str(counter))
            counter += 1
        return feature_list


def getFeaturesNames():
    records = wave.getRecords('All')[0]
    returnMatrix = []

    sig = Signal(records[0], wave.load(records[0]))

    features_names = getFeatures(sig, names=True)
    return features_names


def getFeatures(sig, names=False):
    """
    this function extract the features from the attributes of a signal

    Parameters
    ----------
    sig : Signal object
        instantiated signal object of class Signal
    names: boolean
        a parameter to choose whether to output the columns or not
        if set to true, will return a list of column names.
        However, the column names are all hardcoded. Any changes to
        the features will not automatically change the output column names

    Returns
    -------
    features : list (not numpy array)
        a feature list for the given signal

    feature names :  list
        feature list
    """

    features = [sig.name]

    features += list(sig.RRbinsN)
    features.append(np.var(sig.RRintervals))

    features.append(wave.calculate_residuals(sig.data))

    wtcoeff = pywt.wavedecn(sig.data, 'sym5', level=5, mode='constant')
    wtstats = wave.stats_feat(wtcoeff)
    features += wtstats.tolist()

    features.append(wave.diff_var(sig.RRintervals.tolist()))
    features.append(wave.diff_var(sig.RRintervals.tolist(), skip=3))

    # TODO: do cal_stats for all these features?

    features.append(np.mean(sig.PHeights))
    features.append(np.var(sig.PHeights))
    features += list(sig.PHeightbinsN)

    features.append(np.mean(sig.PPintervals))
    features.append(np.var(sig.PPintervals))
    features += list(sig.PPbinsN)

    features.append(np.mean(sig.PRintervals))
    features.append(np.var(sig.PRintervals))
    features += list(sig.PRbinsN)

    if names == True:
        feature_names = createcolnames(len([sig.name]), 'Name')
        feature_names += createcolnames(len(list(sig.RRbinsN)), 'RR_bins_')

        feature_names += createcolnames(len([np.var(sig.RRintervals)]), 'Var_RR_intervals')

        feature_names += createcolnames(len([wave.calculate_residuals(sig.data)]), 'Residuals')

        feature_names += createcolnames(len(list(wtstats.tolist())), 'wavelet_coeff_')
        feature_names += createcolnames(len([wave.diff_var(sig.RRintervals.tolist())]), 'RRintervals_every_2nd')
        feature_names += createcolnames(len([wave.diff_var(sig.RRintervals.tolist())]), 'RRintervals_every_3rd')

        feature_names += createcolnames(len([np.mean(sig.PHeights)]), 'Mean_PHeights')
        feature_names += createcolnames(len([np.var(sig.PHeights)]), 'Var_PHeights')
        feature_names += createcolnames(len(list(sig.PHeightbinsN)), 'PheightBins_')

        feature_names += createcolnames(len([np.mean(sig.PPintervals)]), 'Mean_PPintervals')
        feature_names += createcolnames(len([np.var(sig.PPintervals)]), 'Var_PPintervals')
        feature_names += createcolnames(len(list(sig.PPbinsN)), 'PPBinsN_')

        feature_names += createcolnames(len([np.mean(sig.PRintervals)]), 'Mean_PRintervals')
        feature_names += createcolnames(len([np.var(sig.PRintervals)]), 'Var_PRintervals')
        feature_names += createcolnames(len(list(sig.PRbinsN)), 'PRBinsN_')
        return feature_names

    return features


def saveSignalFeatures():
    """
    This function saves all the features for each signal into a giant dataframe
    This is so we don't have to re-derive the peaks, intervals, etc. for each signal

    Parameters
    ----------
    None

    Returns
    -------
    Saves dataframe as hardcoded_features.csv where each row is a filtered signal with the getFeatures() features
    """

    records = wave.getRecords('All')[0]
    returnMatrix = []

    for i in records:
        sig = Signal(i, wave.load(i))

        features = getFeatures(sig)
        features = features[1:]
        returnMatrix.append(features)
    return returnMatrix




